package com.idanhahn.usbrelay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ToggleButton;


public class MainActivity extends ActionBarActivity {

	private static final String TAG = "usbrelay_debugmode";
	private static char[] status;
	
	// declare buttons:
	ToggleButton relay1	;
	ToggleButton relay2	;
	ToggleButton relay3	;
	ToggleButton relay4	;
	ToggleButton relay5	;
	ToggleButton relay6	;
	ToggleButton relay7	;
	ToggleButton relay8	;
	ToggleButton relay9	;
	ToggleButton relay10;
	ToggleButton relay11;
	ToggleButton relay12;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataBaseConnectorUSB dataBaseConnectorUSB = new DataBaseConnectorUSB();
        dataBaseConnectorUSB.execute();

        relay1 	= (ToggleButton)findViewById(R.id.relayButton1);
        relay2 	= (ToggleButton)findViewById(R.id.relayButton2);      
        relay3 	= (ToggleButton)findViewById(R.id.relayButton3);
        relay4 	= (ToggleButton)findViewById(R.id.relayButton4);
        relay5 	= (ToggleButton)findViewById(R.id.relayButton5);
        relay6 	= (ToggleButton)findViewById(R.id.relayButton6);
        relay7 	= (ToggleButton)findViewById(R.id.relayButton7);      
        relay8 	= (ToggleButton)findViewById(R.id.relayButton8);
        relay9 	= (ToggleButton)findViewById(R.id.relayButton9);
        relay10 = (ToggleButton)findViewById(R.id.relayButton10);      
        relay11 = (ToggleButton)findViewById(R.id.relayButton11);
        relay12 = (ToggleButton)findViewById(R.id.relayButton12);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    // Class handles usbRelay
    private class DataBaseConnectorUSB extends AsyncTask<Void, Void, String>{
        
        @Override
        protected String doInBackground(Void... params) {
        	
        	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        	nameValuePairs.add(new BasicNameValuePair("year","1980"));
        	
        	String status = null;
        	String result = null;
        	
        	InputStream is = null;
        	
        	// send post request to DB
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://idanhahn.info.tm/usbRelayApp/getRelayStatus.php");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost);
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();
        	
        	}catch(Exception e){
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) {
        	                sb.append(line + "\n");
        	        }
        	        is.close();
        	        result=sb.toString();
        	}catch(Exception e){
        	        Log.e("log_tag", "Error converting result "+e.toString());
        	}
        	 
        	//parse json data
        	try{
        	        JSONObject json_data = new JSONObject(result); 
        	        Log.i("log_tag","status: "+json_data.getString("status"));
        	        status = json_data.getString("status").replace("status: ", "");
        	        Log.i("log_tag","status: "+status);
        	}catch(JSONException e){
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	}
			return status;
        }
        
        @Override
        protected void onPostExecute(String result) {
        	super.onPostExecute(result);
        
        	// parse result to get each relay status:
        	
			// set toggle button state
        	ToggleButton relayBTN0 = (ToggleButton) findViewById(R.id.relayButton1);
        	ToggleButton relayBTN1 = (ToggleButton) findViewById(R.id.relayButton2);
        	ToggleButton relayBTN2 = (ToggleButton) findViewById(R.id.relayButton3);
        	ToggleButton relayBTN3 = (ToggleButton) findViewById(R.id.relayButton4);
       
        	Log.i(TAG,"Status recived from server: " + result);
        	result =  new StringBuilder(result).reverse().toString();
        	status = result.toCharArray();
        	
        	if (status[0] == '1'){
        		relayBTN0.setChecked(true);
        	}
        	if (status[1] == '1'){
        		relayBTN1.setChecked(true);
        	}
        	if (status[2] == '1'){
        		relayBTN2.setChecked(true);
        	}
        	if (status[3] == '1'){
        		relayBTN3.setChecked(true);
        	}
        	
        }
    }
    
    
    // Class handles Arduino Relay
    private class DataBaseConnectorArduino extends AsyncTask<Void, Void, String>{
        
        @Override
        protected String doInBackground(Void... params) {
        	
        	String status = null;
        	String result = null;
        	
        	InputStream is = null;
        	
        	// send post request to DB
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://idanhahn.info.tm:81/STAT/");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost);
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();
        	
        	}catch(Exception e){
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) {
        	                sb.append(line + "\n");
        	        }
        	        is.close();
        	        result=sb.toString();
        	}catch(Exception e){
        	        Log.e("log_tag", "Error converting result "+e.toString());
        	}
        	 
        	//parse json data
        	try{
        	        JSONObject json_data = new JSONObject(result); 
        	        Log.i("log_tag","status: "+json_data.getString("status"));
        	        status = json_data.getString("status").replace("status: ", "");
        	        Log.i("log_tag","status: "+status);
        	}catch(JSONException e){
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	}
			return status;
        }
        
        @Override
        protected void onPostExecute(String result) {
        	super.onPostExecute(result);
        
        	// parse result to get each relay status:
        	
			// set toggle button state
        	ToggleButton relayBTN0 = (ToggleButton) findViewById(R.id.relayButton1);
        	ToggleButton relayBTN1 = (ToggleButton) findViewById(R.id.relayButton2);
        	ToggleButton relayBTN2 = (ToggleButton) findViewById(R.id.relayButton3);
        	ToggleButton relayBTN3 = (ToggleButton) findViewById(R.id.relayButton4);
       
        	Log.i(TAG,"Status recived from server: " + result);
        	result =  new StringBuilder(result).reverse().toString();
        	status = result.toCharArray();
        	
        	if (status[0] == '1'){
        		relayBTN0.setChecked(true);
        	}
        	if (status[1] == '1'){
        		relayBTN1.setChecked(true);
        	}
        	if (status[2] == '1'){
        		relayBTN2.setChecked(true);
        	}
        	if (status[3] == '1'){
        		relayBTN3.setChecked(true);
        	}
        	
        }
    }
    
    
    
    
    
    
    
    
    
    
    public void onToggleClicked(View view){
    	// this method is used by all 4 toggle buttons,
    	// method check which button had been clicked, modify status string and execute usbRelay server
    	switch (view.getId()){
    	case R.id.relayButton1:
        	Log.i(TAG,"Button 1 clicked");
        	status[0] = (status[0] == '0') ? '1' : '0';
    		break;
    	case R.id.relayButton2: 
        	Log.i(TAG,"Button 2 clicked");
        	status[1] = (status[1] == '0') ? '1' : '0';
    		break;
    	case R.id.relayButton3:
        	Log.i(TAG,"Button 3 clicked");
        	status[2] = (status[2] == '0') ? '1' : '0';
    		break;
    	case R.id.relayButton4:
        	Log.i(TAG,"Button 4 clicked");
        	status[3] = (status[3] == '0') ? '1' : '0';
    		break;
    	default:
    		break;
    	}
    	
        Log.i(TAG,"new status: " + new String (status));
    	// handle URL request:
    	new Thread (new Runnable(){

			@Override
			public void run() {
	    		String statusString = new String(status);
	    		HttpClient client = new DefaultHttpClient();
	    		
	    		Log.i(TAG,"sending Get request http://idanhahn.info.tm/usbRelayApp/setRelayStatus.php?status="+statusString);
	    		HttpGet request = new HttpGet("http://idanhahn.info.tm/usbRelayApp/setRelayStatus.php?status="+statusString);
	    		try {
					client.execute(request);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
    		
    	}).start();
    }
}
